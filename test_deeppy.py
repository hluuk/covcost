import numpy as np
import cudarray as ca
import deeppy as dp
import deeppy.expr as ex
import covcost_deeppy

nsample = 10
nfeature = 64**2
x_shape = (nsample, nfeature)

# Source is defined in deeppy_mod/deeppy/expr/base.py
target = ex.Source.from_array(np.reshape(np.random.uniform(-1, 1, nsample * nfeature), x_shape))

pred = ex.Source.from_array(np.reshape(np.random.uniform(-1, 1, nsample * nfeature), x_shape))
# gradient of the prediction will be stored in pred.grad_array by the loss function's bprop
# and it must match the size of the prediction
pred.grad_array = ca.zeros(pred.shape)

# ~/dev/deeppy_mod/deeppy/expr/nnet/loss.py
sqerr = ex.nnet.loss.SquareError()
sqerr.__call__(pred, target)
sqerr.setup()
sqerr.fprop()
sqerr.bprop()
# check the error gradient
print "SquareError of prediction: %s" % sqerr.array
print "Norm of the prediction SquareError gradient matrix: %.2f" % np.linalg.norm(sqerr.pred.grad_array)

covsqerr = covcost_deeppy.CovSqError()
covsqerr.__call__(pred, target)
covsqerr.setup()
covsqerr.fprop()
covsqerr.bprop()
print "CovSqError of prediction: %s" % covsqerr.array
print "Norm of the prediction CovSqError gradient matrix: %.2f" % np.linalg.norm(covsqerr.pred.grad_array)


covabserr = covcost_deeppy.CovAbsError()
covabserr.__call__(pred, target)
covabserr.setup()
covabserr.fprop()
covabserr.bprop()
print "CovAbsError of prediction: %s" % covabserr.array
print "Norm of the prediction CovAbsError gradient matrix: %.2f" % np.linalg.norm(covabserr.pred.grad_array)

