# COVARIANCE COST #

Cost function based on covariance.

Useful for autoencoders when reconstruction error seems inadequate.

Note! It is very slow and still a bit shaky.