import numpy as np
from covcost_ca import _costabs, _dcostabs, _costsq, _dcostsq, _costsqindex, _dcostsqindex

import cudarray as ca
import deeppy.expr as ex

class CovSqError(ex.nnet.loss.Loss):
	def __init__(self, index_size = 0):
		'''
		  If index_size > 0, covariance matrix will be calculated 
		  with respect to randomly chosen {index_size} items of input.
		'''
		if index_size < 0:
			raise RuntimeException("[CovSqError.__init__] negative index size")
		self.index_size = index_size
		self.index = None

	'''
	  Squared covariance error 
	'''
	def fprop(self):
		if self.index_size > 0:
			self.index = np.random.randint(0, self.pred.array.shape[-1], self.index_size)
		covcostsq(self.pred.array, self.target.array, self.array, self.index)

	def bprop(self):
		dcovcostsq(self.pred.array, self.target.array, self.pred.grad_array, self.index)

class CovAbsError(ex.nnet.loss.Loss):
	'''
	  Absolute covariance error 
	'''
	def fprop(self):
		covcostabs(self.pred.array, self.target.array, self.array)

	def bprop(self):
		dcovcostabs(self.pred.array, self.target.array, self.pred.grad_array)

def covcostsq(pred, target, out, index = None):
	'''
	  out must be a ca.ndarray of shape (pred.shape[0],)
	'''
	print "[covcostsq] pred.shape %s, target.shape %s, out.shape %s" % (pred.shape, target.shape, out.shape)
	shape = out[0].shape
	for i in range(pred.shape[0]):
		# reshape to one dimension since >2 dimensional tensors are not
		# compatible with the cost function
		x = ca.reshape(pred[i,:], (pred[i,:].size,))
		y = ca.reshape(target[i,:], (target[i,:].size,))
		if index is None:
			out[i] = ca.reshape(_costsq(x, y), shape)
		else:
			out[i] = ca.reshape(_costsqindex(x, y, index), shape)
		

def dcovcostsq(pred, target, out, index = None):
	'''
	  pred and target are matrices were rows correspond to samples
	  and columns correspond to variables
	  
	  out must be a ca.ndarray of shape pred.shape
	'''
	print "[dcovcostsq] pred.shape %s, target.shape %s, out.shape %s" % (pred.shape, target.shape, out.shape)
	shape = out[0].shape
	for i in range(pred.shape[0]):
		# reshape to one dimension since >2 dimensional tensors are not
		# compatible with the cost function
		x = ca.reshape(pred[i,:], (pred[i,:].size,))
		y = ca.reshape(target[i,:], (target[i,:].size,))
		if index is None:
			out[i] = ca.reshape(_dcostsq(x, y), shape)
		else:
			out[i] = ca.reshape(_dcostsqindex(x, y, index), shape)

def covcostabs(pred, target, out):
	'''
      out must be a ca.ndarray of shape (pred.shape[0],)
	'''
	for i in range(pred.shape[0]):
		out[i] = _costabs(pred[i,:], target[i,:])

def dcovcostabs(pred, target, out):
	'''
	  pred and target are matrices were rows correspond to samples
	  and columns correspond to variables
	  
      out must be a ca.ndarray of shape pred.shape
	'''
	for i in range(pred.shape[0]):
		out[i,:] = _dcostabs(pred[i,:], target[i,:])


