'''
  http://docs.sympy.org/dev/tutorial/calculus.html
  http://docs.sympy.org/latest/modules/concrete.html
  http://docs.sympy.org/0.7.2/modules/matrices/matrices.html
  http://docs.sympy.org/0.7.2/modules/matrices/expressions.html

  Sympy does not now how to get derivatives of matrix expressions, however, 
  if you reduce matrix expressions into expressions involving only scalars
  then you will get what is needed 

  we want to get the derivative of the following expression,
  where x and y are column vectors of the same length.


  matrix cookbook: http://www2.imm.dtu.dk/pubdb/views/edoc_download.php/3274/pdf/imm3274.pdf

  Example 1.

  We wanna differentiate the following expression, where x and y are vectors
  of identical length and t(a) is the transpose of a and x*t(x) is the outer product
  of x with itself:
  sum((x*t(x) - y*t(y))**2)

  if len(x) is 3 the expression with respect to x[0] decomposes to the following,
  where a,b,c are x[0], x[1], x[2] and x, y, z are y[0], y[1], y[2]
  (a**2 - x**2)**2 + 2*((a*b - x*y)**2) + 2*((a*c - x*z)**2) 

  and the first derivative wrt x[0] is hence given by
  diff((a**2 - x**2)**2 + 2*((a*b - x*y)**2) + 2*((a*c - x*z)**2), a) 


  Example 2.

  Given cost function:
  sum(abs(x*t(x) - y*t(y)))

  the first derivative wrt x[0] is given by
  diff(Abs(a**2 - x**2) + 2*Abs(a*b - x*y) + 2*Abs(a*c - x*z), a) 
  

'''



from sympy import *
a, b, c, d, e, f, x, y, z, k, m = symbols('a b c, d, e, f, x y z k m', real = True)
init_printing(use_unicode=True)

# squared version
diff((a**2 - x**2)**2 + 2*(a*b - x*y)**2 + 2*(a*c - x*z)**2, a)
'''
    ⎛ 2    2⎞                                    
4⋅a⋅⎝a  - x ⎠ + 4⋅b⋅(a⋅b - x⋅y) + 4⋅c⋅(a⋅c - x⋅z)
'''

# absolute version
diff(Abs(a**2 - x**2) + 2*Abs(a*b - x*y) + 2*Abs(a*c - x*z), a)
'''
        ⎛ 2    2⎞                                            
2⋅a⋅sign⎝a  - x ⎠ + 2⋅b⋅sign(a⋅b - x⋅y) + 2⋅c⋅sign(a⋅c - x⋅z)
'''


