import numpy as np
import pandas
import re
import cudarray as ca

'''
  Profiling indicated that the combined execution time for the forward and backward pass
  is the same for squared and absolute cost functions (around 1 sec).

	In [117]:     %timeit _costsq(x, y)
	1 loops, best of 3: 569 ms per loop

	In [119]:     %timeit _dcostsq(x, y)
	1 loops, best of 3: 414 ms per loop


	In [118]:     %timeit _costabs(x, y)
	1 loops, best of 3: 414 ms per loop

	In [120]:     %timeit _dcostabs(x, y)
	1 loops, best of 3: 569 ms per loop
'''

# ---------------  square cost vanilla version  -----------------

# cudarray implementation 
def _costsq(x, y):
	'''
	  assumes x and y are flat arrays
	'''
	# 338 ms per loop
	covdiff = ca.array(np.outer(x, x) - np.outer(y, y))
	# Note! for some reason, np.sum yields a considerably lower value than ca.sum 
	# sum all elements 
	return ca.sum(ca.power(covdiff, 2))

# numpy implementation 
def _costsq_np(x, y):
	'''
	  assumes x and y are flat arrays
	'''
	# 338 ms per loop
	covdiff = np.outer(x, x) - np.outer(y, y)
	# Note! for some reason, np.sum yields a considerably lower value than ca.sum 
	return np.sum(np.power(covdiff, 2))

def _dcostsq(x, y):
	'''
	  Gradient of _costsq(x,y)
	'''
	covdiff = ca.array(np.outer(x, x) - np.outer(y, y))
	# multiply each row of matrix (xcov-ycov) by 4x
	result = 4 * x * covdiff 
	# row sums
	return ca.sum(result, 1)

# ---------------  square cost indexed version  -----------------

def _cov_with_index(x, index):
	'''
	  get the covariance of each item of x with x[index]
	'''
	x_index = np.ones((len(x), len(index)+1), dtype = np.float32)
	# row-wise multiply
	x_index[:,1:] = x_index[:,1:] * x[index]
	# replace first column with x
	x_index[:,0] = x
	# column-wise multiply 
	return x_index * np.reshape(x,(len(x),1))

def _costsqindex(x, y, index):
	'''
	  assumes x, y, index are flat arrays
	'''
	# must convert to numpy since cudarray does not support indexing
	x = np.array(x)
	y = np.array(y)
	covdiff = ca.array(_cov_with_index(x, index) - _cov_with_index(y, index))
	# Note! for some reason, np.sum yields a considerably lower value than ca.sum 
	return ca.sum(ca.power(covdiff, 2))

def _dcostsqindex(x, y, index):
	'''
	  assumes x, y, index are flat arrays
	'''
	# must convert to numpy since cudarray does not support indexing
	x = np.array(x)
	y = np.array(y)
	covdiff = _cov_with_index(x, index) - _cov_with_index(y, index)
	# multiply first col of {covdiff} by 4x
	covdiff[:,0] *= 4 * x
	# multiply each row of {covdiff[:,1:]} by {4x[index]}
	covdiff[:,1:] *= 4 * x[index] 
	# row sums
	return ca.array(np.sum(covdiff, 1))

# ---------------  absolute cost  -----------------

def _costabs(x, y):
	'''
	  assumes x and y are flat arrays
	'''
	# 184 ms per loop
	return ca.array(np.sum(np.abs(np.outer(x, x) - np.outer(y, y))) / len(x))

# return the derivative of _costsq
# with respect to each element of x
def _dcostabs(x, y):
	xcov = np.outer(x, x)
	ycov = np.outer(y, y) 
	# multiply each column of matrix (xcov-ycov)
	result = 2 * np.array(x) * np.sign(xcov - ycov)
	return ca.array(np.sum(result, 1) / len(x))


def test_cost(data_file = 'sq55.txt', costf = _costsq, dcostf = _dcostsq):
	'''
	  test cost and gradients output by the R's generate_cost (covcost.R)

	  Example:
	  import covcost as cc
	  cc.test_cost('sq1.txt', costf = cc._costsq, dcostf = cc._dcostsq)
	  cc.test_cost('sq55.txt', costf = cc._costsq, dcostf = cc._dcostsq)
	  cc.test_cost('abs1.txt', costf = cc._costabs, dcostf = cc._dcostabs)
	  cc.test_cost('abs333.txt', costf = cc._costabs, dcostf = cc._dcostabs)
	'''
	xy = pandas.read_table('xy.txt', sep="\t")
	x = xy['x'].values
	y = xy['y'].values
	n = int(re.sub('[a-zA-Z\.]', '', data_file)) - 1

	data = pandas.read_table(data_file, sep="\t")

	cost = []
	dcost = []
	for xn in data['xn']:
		xcopy = x.copy()
		xcopy[n] = xn
		cost.append(costf(xcopy, y))
		dcost.append(dcostf(xcopy, y)[n])

	# should be of the order e-09
	delta_dcost = sum(abs(data['dcost'] - dcost))/len(dcost)

	# should be of the order e-12
	delta_cost = sum(abs(data['cost'] - cost))/len(cost)

	print "Average delta from correct cost and derivative is %.2E and %.2E, respectively" % (delta_cost, delta_dcost)

def test_gradient(file_name = "gradient.txt"):
	'''
	  test gradient as output by R's generate gradient (covcost.R) 

	  read gradients generated using R's generate_gradient() (covcost.R)
	  and compare to python implementation
	  Average delta should be in the order of E-16
	'''
	data = pandas.read_table(file_name, sep="\t")
	x = data['x'].values
	y = data['y'].values
	delta_dcostabs = sum(abs(data['dcostabs'] - _dcostabs(x,y)))/len(x)
	delta_dcostsq = sum(abs(data['dcostsq'] - _dcostsq(x,y)))/len(x)
	print "Average delta from correct derivative is %.2E (squared cost) and %.2E (absolute cost)" % (delta_dcostsq, delta_dcostabs)


def profile():
	x = np.random.uniform(-1.5, 1.5, 64**2)
	y = np.random.uniform(-1.5, 1.5, 64**2)
	#%timeit _costsq(x, y)
	#%timeit _costabs(x, y)
	#%timeit _dcostsq(x, y)
	#%timeit _dcostabs(x, y)
